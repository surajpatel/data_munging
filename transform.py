'''Clean the data'''
class Transform:
    '''contain one method to clean data'''
    def __init__(self, data_form, col1, col2, col3):
        self.data_form = data_form
        self.col1 = col1
        self.col2 = col2
        self.col3 = col3
    def clean_data(self):
        '''perform data clean'''
        clean_data = list()
        index_1 = self.data_form[0].index(self.col1)
        index_2 = self.data_form[0].index(self.col2)
        index_3 = self.data_form[0].index(self.col3)
        for per_row in self.data_form:
            every_row = list()
            if len(per_row) > 1:
                every_row.append(per_row[index_1])
                every_row.append(per_row[index_2].replace("*", ""))
                every_row.append(per_row[index_3].replace("*", ""))
                clean_data.append(every_row)
        return clean_data
