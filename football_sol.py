'''Use solid'''
import common_functionality
import transform
import calculate_diff
class Football:
    '''calculate minimum diff between goals'''
    def __init__(self, file_name):
        self.file_name = file_name
    def diff_goals(self):
        '''return minimum difference and team name'''
        object_of_common_fun = common_functionality.CommonFun(self.file_name)
        football_data = object_of_common_fun.extract_data()
        football_clean_data = transform.Transform(football_data, 'Team', 'F', 'A')
        football_clean_data_in_order = football_clean_data.clean_data()
        return calculate_diff.Difference(football_clean_data_in_order).diff()
