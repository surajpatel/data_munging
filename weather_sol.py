'''Use solid'''
import common_functionality
import transform
import calculate_diff
class  Weather:
    '''class to calculate weather'''
    def __init__(self, file_name):
        self.file_name = file_name
    def min_max_temp(self):
        '''function return weather per day min max'''
        object_of_common_fun = common_functionality.CommonFun(self.file_name)
        weather_data_object = object_of_common_fun.extract_data()
        weather_clean_data = transform.Transform(weather_data_object, 'Dy', 'MxT', 'MnT')
        weather_clean_data_in_order = weather_clean_data.clean_data()
        return calculate_diff.Difference(weather_clean_data_in_order).diff()
