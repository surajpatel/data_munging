'''Use common Functionality for parse data'''
class CommonFun:
    '''return extract data from the file'''
    def __init__(self, file_name):
        self.file_name = file_name
    def extract_data(self):
        '''return a list containing rows of file'''
        data_by_row = list()
        for row_in_file in open(self.file_name, 'r'):
            split_row_in_file = row_in_file.rstrip().split()
            first_element = list()
            if "-" in split_row_in_file:
                split_row_in_file.remove("-")
                first_element = split_row_in_file[0].split(".")
            if len(first_element) > 0 and first_element[0].isdigit():
                del split_row_in_file[0]
            data_by_row.append(split_row_in_file)
        return data_by_row
    