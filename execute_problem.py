'''saperate file for execute different problems'''
import weather_sol
import football_sol

def execute_problems():
    '''execute programs'''
    weather = weather_sol.Weather('data/weather.dat')
    minimum_weather_diff = weather.min_max_temp()
    print("Day {}, Temperature {}".format(minimum_weather_diff[0], minimum_weather_diff[1]))
    football = football_sol.Football('data/football.dat')
    min_goal_diff = football.diff_goals()
    print("Team {}, goal_diff {}".format(min_goal_diff[0], min_goal_diff[1]))
execute_problems()
