'''Use to difference'''
class Difference:
    '''contain diff method'''
    def __init__(self, data_container):
        self.data_container = data_container
    def diff(self):
        '''calculate differences'''
        temp_differeces = 1000
        count = 0
        temp_main = 0
        for row in self.data_container:
            count += 1
            if count > 1:
                diff = abs(float(row[1]) - float(row[2]))
                if diff < temp_differeces:
                    temp_differeces = diff
                    temp_main = row[0]
        return temp_main, temp_differeces
