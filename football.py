'''spike weather problem'''
def smallest_diff_in_f_a(file_name):
    '''return team name whos difference is minimum'''
    count = 0
    absolute_difference = 100
    team_name = ""
    for football in open(file_name, 'r'):
        per_team_record = football.split()
        count += 1
        if count > 1 and count != 19:
            difference_between_f_a = abs(int(per_team_record[6]) - int(per_team_record[8]))
            if difference_between_f_a < absolute_difference:
                absolute_difference = difference_between_f_a
                team_name = per_team_record[1]
    return team_name, absolute_difference
def execute_smallest_diff_in_f_a():
    '''function to execute problem'''
    print(smallest_diff_in_f_a('data/football.dat'))
execute_smallest_diff_in_f_a()
