'''spike weather problem'''
def max_min_temperature(file_name):
    '''function to return per day min max temperature'''
    count = 0
    day_number = 0
    temp_diff = 100
    for weather in open(file_name, 'r'):
        weather_per_day = weather.split()
        count += 1
        if count > 2 and weather_per_day[0].isnumeric():
            max_temp = weather_per_day[1].replace("*", '')
            min_temp = weather_per_day[2].replace("*", '')
            diff = abs(int(max_temp) - int(min_temp))
            if diff < temp_diff:
                temp_diff = diff
                day_number = weather_per_day[0]
    return day_number, temp_diff

def execute_weather():
    '''execute problem'''
    print(max_min_temperature('data/weather.dat'))
execute_weather()
